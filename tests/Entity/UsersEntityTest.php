<?php

namespace App\Tests\Entity;

use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\ValidatorInterface;
use App\Entity\Users;

class UsersEntityTest extends KernelTestCase
{

    public function getEntity(): Users {
        return (new Users())
        ->setNom("Ramery")
        ->setPrenom("Matthias")
        ->setEmail("matthias.rameryramery@gmail.com")
        ->setTel("0661259842")
        ->setSexe("M")
        ->setDateEnregistrement(\DateTime::createFromFormat('Y-m-d', "2022-02-09"))
        ->setProfilStatut(1)
        ->setPassword("test")
        ->setCodePostal("62138")
        ->setVille("Violaines")
        ->setNotifications(1)
        ->setAdresse("48 rue des peupliers");
    }

    public function assertHasErrors(Users $user, int $number = 0){
        self::bootKernel();
        $error = self::$container->get('validator')->validate($user);
        $this->assertCount($number, $error);
    }

    public function testUsersEntityIsValid() {
        $this->assertHasErrors($this->getEntity(), 0);
    }

    public function testUsersEntityIsInvalid() {
        $this->assertHasErrors($this->getEntity()->setTel(""), 1);
        $this->assertHasErrors($this->getEntity()->setTel("062683256a"), 1);
        $this->assertHasErrors($this->getEntity()->setTel("1251121200000000000000"), 1);
    }
}

