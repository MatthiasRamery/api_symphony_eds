<?php

namespace App\Tests\Entity;

use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class GetDrivingHoursTest extends WebTestCase
{
    public function testGetDrivingHours()
    {
        $client = static::createClient();
        $client->request('GET', 'http://localhost:8000/apip/driving_hours');

        $this->assertResponseIsSuccessful();
    }
}