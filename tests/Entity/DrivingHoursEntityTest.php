<?php

namespace App\Tests\Entity;

use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\ValidatorInterface;
use App\Entity\DrivingHours;

class DrivingHoursEntityTest extends KernelTestCase
{

    public function getEntity(): DrivingHours {
        return (new DrivingHours())
            ->setDate(\DateTime::createFromFormat('Y-m-d', "2022-06-12"))
            ->setStartTime(\DateTime::createFromFormat('H:m:s', "10:00:00"))
            ->setEndTime(\DateTime::createFromFormat('H:m:s', "11:00:00"))
            ->setStatus(true);
    }

    public function assertHasErrors(DrivingHours $hour, int $number = 0){
        self::bootKernel();
        $error = self::$container->get('validator')->validate($hour);
        $this->assertCount($number, $error);
    }

    public function testDrivingHoursEntityIsValid() {
        $this->assertHasErrors($this->getEntity(), 0);
    }

    public function testDrivingHoursEntityIsInvalid() {
        $this->assertHasErrors($this->getEntity()->setDate(\DateTime::createFromFormat('Y-m-d', "2022-05-09")), 1);
    }
}

