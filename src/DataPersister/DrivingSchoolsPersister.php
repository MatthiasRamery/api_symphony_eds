<?php

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use App\Entity\DrivingSchools;
use Doctrine\ORM\EntityManagerInterface;

class DrivingSchoolsPersister implements DataPersisterInterface
{
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function supports($data): bool
    {
        return $data instanceof DrivingSchools;
    }

    public function persist($data)
    {
        $data->setDateEnregistrement(new \DateTime());

        $this->entityManager->persist($data);
        $this->entityManager->flush();
    }

    public function remove($data)
    {
        $this->entityManager->remove($data);
        $this->entityManager->flush();
    }
}