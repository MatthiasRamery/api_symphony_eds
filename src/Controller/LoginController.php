<?php 

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Security\ApiKeyAuthenticator;
use App\Repository\UsersRepository;
use App\Entity\Users;

class LoginController extends AbstractController {
    /**
     * @Route("/loginAdmin", name="loginAdmin", methods={"POST"})
     */
    public function index(Request $request, UsersRepository $usersRepository): Response
    {
        $userToIdentify = json_decode($request->getContent());

        $user = $usersRepository->findOneBy(array('email' => $userToIdentify->email, 'password' => md5($userToIdentify->password), 'role'=> 6));
        $isUser = false;

        if(!empty($user)){
            $user = array("id" => $user->getId(), "notifs" => $user->getNotifications(), "email" => $user->getEmail());            
            $isUser = true;
        }

       if($isUser){
        $donnees = ["utilisateurExistant" => $isUser, "user" => $user];
       }else{
        $donnees = ["utilisateurExistant" => $isUser];
       }

        return new JsonResponse($donnees);

    }

    /**
     * @Route("/login", name="login", methods={"POST"})
     */
    public function indexMobile(Request $request, UsersRepository $usersRepository): Response
    {
        $userToIdentify = json_decode($request->getContent());
        $user = $usersRepository->findOneBy(array('email' => $userToIdentify->email, 'password' => md5($userToIdentify->password), 'role' => 4));
        if(empty($user)){
            $user = $usersRepository->findOneBy(array('email' => $userToIdentify->email, 'password' => md5($userToIdentify->password), 'role' => 5));
        }
        $isUser = false;
        if(!empty($user)){
            $user = array("id" => $user->getId(), "notifs" => $user->getNotifications(), "email" => $user->getEmail());
            $isUser = true;
        }

       if($isUser){
        $donnees = ["utilisateurExistant" => $isUser, "user" => $user];
       }else{
        $donnees = ["utilisateurExistant" => $isUser];
       }

        return new JsonResponse($donnees);

    }
}