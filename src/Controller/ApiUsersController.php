<?php

namespace App\Controller;

use App\Entity\Users;
use App\Repository\UsersRepository;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;

class ApiUsersController extends AbstractController
{
    /**
     * @Route("/api/users", name="api_users", methods={"GET"})
     */
    public function index(UsersRepository $usersRepository): Response
    {
        $users = $usersRepository->findAll();

        return $this->json($users, 200, [], ['groups' => 'users:read']);

    }

    /**
     * @Route("/api/users", name="api_post_user", methods={"POST"})
     */
    public function createUser(Request $request, EntityManagerInterface $entityManager){

        $serializer = $this->get("serializer");
        $json = $request->getContent();

        try{

            $user = $serializer->deserialize($json, Users::class, 'json');
            $user->setDateEnregistrement(new \DateTime());

            $entityManager->persist($user);
            $entityManager->flush();

            return $this->json($user, 201, [], ['groups' => 'users:read']);

        }catch (NotEncodableValueException $e){

            return $this->json([
                'status' => 400,
                'message' => $e->getMessage()
            ], 400);
        }



    }
}
