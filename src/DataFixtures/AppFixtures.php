<?php

namespace App\DataFixtures;

use App\Entity\DrivingHours;
use App\Entity\DrivingSchools;
use App\Entity\Role;
use App\Entity\Users;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $role1 = new Role();
        $role1->setDescription("Élève");
        $manager->persist($role1);

        $role2 = new Role();
        $role2->setDescription("Moniteur");
        $manager->persist($role2);

        $role3 = new Role();
        $role3->setDescription("Admin");
        $manager->persist($role3);

        for ($p = 0; $p < 10; $p++) {
            $driving_school = new DrivingSchools();
            $user = new Users;
            $driving_hour = new DrivingHours();
            if($p==1){
                $role = $role2;
            }
            else{
                $role = $role1;
            }
            $driving_school->setName("Driving school n° " . $p)
                ->setAdresse($p . " rue des saules")
                ->setCodePostal("62138")
                ->setVille("Violaines")
                ->setTel("0320" . $p . "59842")
                ->setEmail("driving.school" . $p . "@gmail.com");

            $user->setDrivingSchools($driving_school)
                ->setRole($role)
                ->addDrivingHour($driving_hour)
                ->setNom("Ramery")
                ->setPrenom("Matthias")
                ->setEmail("matthias.ramery" . $p . "@gmail.com")
                ->setTel("0626" . $p . "59842")
                ->setSexe("M")
                ->setDateEnregistrement(\DateTime::createFromFormat('Y-m-d', "2022-0" . $p. "-09"))
                ->setProfilStatut(1)
                ->setPassword("test")
                ->setCodePostal("62138")
                ->setVille("Violaines")
                ->setNotifications(1)
                ->setAdresse($p . " rue des peupliers");

            $driving_hour->setDate(\DateTime::createFromFormat('Y-m-d', "2022-0" . $p. "-09"))
                ->setStartTime(\DateTime::createFromFormat('H:m:s', $p + 10 . ":15:00"))
                ->setEndTime(\DateTime::createFromFormat('H:m:s', $p + 11 . ":15:00"))
                ->setStatus(true)
                ->setUser($user);

            $manager->persist($driving_school);
            $manager->persist($user);
            $manager->persist($driving_hour);
        }

        $manager->flush();
    }
}
