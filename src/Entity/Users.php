<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;


/**
 * Users
 *
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="App\Repository\UsersRepository")
 * @ApiResource(
 *     normalizationContext={"groups"={"users:read"}},
 *     denormalizationContext={"groups"={"users:write"}}
 * )
 * @ApiFilter(SearchFilter::class, properties = {"role"})
 * 
 */
class Users implements UserInterface, PasswordAuthenticatedUserInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"users:read", "driving_hours:read"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=100, nullable=false)
     * @Groups({"users:read", "users:write"})
     * @Assert\NotBlank()
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=100, nullable=false)
     * @Groups({"users:read", "users:write"}) 
     * @Assert\NotBlank()
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=50, nullable=false)
     * @Groups("users:write")
     * @Assert\NotBlank()
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=false)
     * @Groups({"users:read", "users:write"})
     * @Assert\NotBlank()
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="tel", type="string", length=10, nullable=false)
     * @Groups({"users:read", "users:write"})
     * @Assert\NotBlank()
     * @Assert\Regex("/^\d{10}$/")
     */
    private $tel;

    /**
     * @var string
     *
     * @ORM\Column(name="sexe", type="string", length=1, nullable=false)
     * @Groups({"users:read", "users:write"})
     */
    private $sexe;

    /**
     * @var \Date
     *
     * @ORM\Column(name="date_enregistrement", type="date", nullable=false)
     * @Groups({"users:read", "users:write"})
     */
    private $dateEnregistrement;

    /**
     * @var int
     *
     * @ORM\Column(name="profil_statut", type="integer", nullable=false)
     * @Groups({"users:read", "users:write"})
     */
    private $profilStatut;

    /**
     * @var string
     *
     * @ORM\Column(name="code_postal", type="string", length=5, nullable=false)
     * @Groups({"users:read", "users:write"})
     */
    private $codePostal;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=100, nullable=false)
     * @Groups({"users:read", "users:write"})
     */
    private $ville;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=100, nullable=false)
     * @Groups({"users:read", "users:write"})
     */
    private $adresse;

    /**
     * @ORM\OneToMany(targetEntity=DrivingHours::class, mappedBy="user", orphanRemoval=true)
     */
    private $drivingHours;

    /**
     * @ORM\ManyToOne(targetEntity=DrivingSchools::class, inversedBy="users")
     * @ORM\JoinColumn(nullable=true)
     * @Groups({"users:read", "users:write"})
     */
    private $drivingSchools;

    /**
     * @ORM\ManyToOne(targetEntity=Role::class, inversedBy="users", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"users:read", "users:write"})
     */
    private $role;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"users:read", "users:write"})
     */
    private $notifications;

    /**
     * @ORM\ManyToOne(targetEntity=Users::class, inversedBy="eleveAssocie")
     * @Groups({"users:read", "users:write"})
     */
    private $moniteurAssocie;

    /**
     * @ORM\OneToMany(targetEntity=Users::class, mappedBy="moniteurAssocie")
     */
    private $eleveAssocie;

    public function __construct()
    {
        $this->drivingHours = new ArrayCollection();
        $this->eleveAssocie = new ArrayCollection();
    }

    /**
     * @return Collection|DrivingHours[]
     */
    public function getDrivingHours(): Collection
    {
        return $this->drivingHours;
    }

    public function addDrivingHour(DrivingHours $drivingHour): self
    {
        if (!$this->drivingHours->contains($drivingHour)) {
            $this->drivingHours[] = $drivingHour;
            $drivingHour->setUser($this);
        }

        return $this;
    }

    public function removeDrivingHour(DrivingHours $drivingHour): self
    {
        if ($this->drivingHours->removeElement($drivingHour)) {
            // set the owning side to null (unless already changed)
            if ($drivingHour->getUser() === $this) {
                $drivingHour->setUser(null);
            }
        }

        return $this;
    }

    public function getDrivingSchools(): ?DrivingSchools
    {
        return $this->drivingSchools;
    }

    public function setDrivingSchools(?DrivingSchools $drivingSchools): self
    {
        $this->drivingSchools = $drivingSchools;

        return $this;
    }

    public function getRole(): ?Role
    {
        return $this->role;
    }

    public function setRole(?Role $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getTel(): ?string
    {
        return $this->tel;
    }

    public function setTel(string $tel): self
    {
        $this->tel = $tel;

        return $this;
    }

    public function getSexe(): ?string
    {
        return $this->sexe;
    }

    public function setSexe(string $sexe): self
    {
        $this->sexe = $sexe;

        return $this;
    }

    public function getDateEnregistrement(): ?\DateTimeInterface
    {
        return $this->dateEnregistrement;
    }

    public function setDateEnregistrement(\DateTimeInterface $dateEnregistrement): self
    {
        $this->dateEnregistrement = $dateEnregistrement;

        return $this;
    }

    public function getProfilStatut(): ?bool
    {
        return $this->profilStatut;
    }

    public function setProfilStatut(bool $profilStatut): self
    {
        $this->profilStatut = $profilStatut;

        return $this;
    }

    public function getCodePostal(): ?string
    {
        return $this->codePostal;
    }

    public function setCodePostal(string $codePostal): self
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

        /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

        /**
     * @deprecated since Symfony 5.3
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getNotifications(): ?bool
    {
        return $this->notifications;
    }

    public function setNotifications(bool $notifications): self
    {
        $this->notifications = $notifications;

        return $this;
    }

    public function getMoniteurAssocie(): ?self
    {
        return $this->moniteurAssocie;
    }

    public function setMoniteurAssocie(?self $moniteurAssocie): self
    {
        $this->moniteurAssocie = $moniteurAssocie;

        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getEleveAssocie(): Collection
    {
        return $this->eleveAssocie;
    }

    public function addEleveAssocie(self $eleveAssocie): self
    {
        if (!$this->eleveAssocie->contains($eleveAssocie)) {
            $this->eleveAssocie[] = $eleveAssocie;
            $eleveAssocie->setMoniteurAssocie($this);
        }

        return $this;
    }

    public function removeEleveAssocie(self $eleveAssocie): self
    {
        if ($this->eleveAssocie->removeElement($eleveAssocie)) {
            // set the owning side to null (unless already changed)
            if ($eleveAssocie->getMoniteurAssocie() === $this) {
                $eleveAssocie->setMoniteurAssocie(null);
            }
        }

        return $this;
    }


}
