<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * DrivingHours
 *
 * @ORM\Table(name="driving_hours")
 * @ORM\Entity(repositoryClass="App\Repository\DrivingHoursRepository")
 * @ApiResource(
 *     normalizationContext={"groups"={"driving_hours:read"}},
 *     denormalizationContext={"groups"={"driving_hours:write"}}
 * )
 * @ApiFilter(SearchFilter::class, properties = {"user"})
 */
class DrivingHours
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups("driving_hours:read")
     */
    private $id;

    /**
     * @var \Date
     *
     * @ORM\Column(name="date", type="date", nullable=false)
     * @Groups({"driving_hours:read", "driving_hours:write"})
     * @Assert\GreaterThan("today UTC")
     */
    private $date;

    /**
     * * @JMS\Type("DateTime<'H:m:s'>")
     *
     * @ORM\Column(name="start_time", type="time", nullable=false)
     * @Groups({"driving_hours:read", "driving_hours:write"})
     */
    private $startTime;

    /**
     * @JMS\Type("DateTime<'H:m:s'>")
     *
     * @ORM\Column(name="end_time", type="time", nullable=false)
     * @Groups({"driving_hours:read", "driving_hours:write"})
     */
    private $endTime;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean", nullable=false)
     * @Groups({"driving_hours:read", "driving_hours:write"})
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity=Users::class, inversedBy="drivingHours")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"driving_hours:read", "driving_hours:write"})
     */
    private $user;

    public function getUser(): ?Users
    {
        return $this->user;
    }

    public function setUser(?Users $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTime
    {
        return $this->date;
    }

    public function setDate(\DateTime $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getStartTime(): ?\DateTime
    {
        return $this->startTime;
    }

    public function setStartTime(\DateTime $startTime): self
    {
        $this->startTime = $startTime;

        return $this;
    }

    public function getEndTime(): \DateTime
    {
        return $this->endTime;
    }

    public function setEndTime(\DateTime $endTime): self
    {
        $this->endTime = $endTime;

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }


}
