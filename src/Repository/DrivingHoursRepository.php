<?php

namespace App\Repository;

use App\Entity\DrivingHours;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DrivingHours|null find($id, $lockMode = null, $lockVersion = null)
 * @method DrivingHours|null findOneBy(array $criteria, array $orderBy = null)
 * @method DrivingHours[]    findAll()
 * @method DrivingHours[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DrivingHoursRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DrivingHours::class);
    }

    // /**
    //  * @return DrivingHours[] Returns an array of DrivingHours objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DrivingHours
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
