<?php

namespace App\Repository;

use App\Entity\DrivingSchools;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DrivingSchools|null find($id, $lockMode = null, $lockVersion = null)
 * @method DrivingSchools|null findOneBy(array $criteria, array $orderBy = null)
 * @method DrivingSchools[]    findAll()
 * @method DrivingSchools[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DrivingSchoolsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DrivingSchools::class);
    }

    // /**
    //  * @return DrivingSchools[] Returns an array of DrivingSchools objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DrivingSchools
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
